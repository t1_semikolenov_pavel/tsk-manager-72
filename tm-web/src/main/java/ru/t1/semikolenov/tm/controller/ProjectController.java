package ru.t1.semikolenov.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.semikolenov.tm.enumerated.Status;
import ru.t1.semikolenov.tm.model.Project;
import ru.t1.semikolenov.tm.service.ProjectService;
import ru.t1.semikolenov.tm.util.UserUtil;

import java.util.List;

@Controller
public class ProjectController {

    @NotNull
    @Autowired
    private ProjectService projectService;

    @NotNull
    private List<Project> getProjects() {
        return projectService.findAllByUserId(UserUtil.getUserId());
    }

    @ModelAttribute("statuses")
    public Status[] getStatuses() {
        return Status.values();
    }

    @GetMapping("/project/create")
    public String create() {
        projectService.add("new project " + System.currentTimeMillis(), UserUtil.getUserId());
        return "redirect:/projects";
    }

    @GetMapping("/project/delete/{id}")
    public String delete(@PathVariable("id") final String id) {
        projectService.removeByIdAndUserId(id, UserUtil.getUserId());
        return "redirect:/projects";
    }

    @PostMapping("/project/edit/{id}")
    public String edit(@ModelAttribute("project") final Project project, final BindingResult result) {
        projectService.save(project, UserUtil.getUserId());
        return "redirect:/projects";
    }

    @GetMapping("/project/edit/{id}")
    public ModelAndView edit(@PathVariable("id") final String id) {
        final Project project = projectService.findByIdAndUserId(id, UserUtil.getUserId());
        return new ModelAndView("project-edit", "project", project);
    }

    @GetMapping("/projects")
    public ModelAndView list() {
        return new ModelAndView("project-list", "projects", getProjects());
    }

}
