package ru.t1.semikolenov.tm.exception.entity;

import ru.t1.semikolenov.tm.exception.AbstractException;

public final class TaskNotFoundException extends AbstractException {

    public TaskNotFoundException() {
        super("Error! Task not found...");
    }

}
