package ru.t1.semikolenov.tm.exception.field;

import ru.t1.semikolenov.tm.exception.AbstractException;

public final class ExistsLoginException extends AbstractException {

    public ExistsLoginException() {
        super("Error! Login already exists...");
    }

}
