package ru.t1.semikolenov.tm.exception.field;

import ru.t1.semikolenov.tm.exception.AbstractException;

public final class EmptyLoginException extends AbstractException {

    public EmptyLoginException() {
        super("Error! Login is empty...");
    }

}
