package ru.t1.semikolenov.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;

public enum Status {

    NOT_STARTED("Not started"),
    IN_PROGRESS("In progress"),
    COMPLETED("Completed");

    @Getter
    @NotNull
    private final String displayName;

    Status(@NotNull final String displayName) {
        this.displayName = displayName;
    }

    @NotNull
    public static String toName(final Status status) {
        if (status == null) return "";
        return status.getDisplayName();
    }

    @NotNull
    public static Status toStatus(final String value) {
        if (value == null || value.isEmpty()) return NOT_STARTED;
        for (final Status status : values()) {
            if (status.name().equals(value)) return status;
        }
        return NOT_STARTED;
    }

}
