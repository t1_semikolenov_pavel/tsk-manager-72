package ru.t1.semikolenov.tm.integration.client;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;
import ru.t1.semikolenov.tm.marker.WebCategory;
import ru.t1.semikolenov.tm.model.Result;
import ru.t1.semikolenov.tm.model.Task;

import java.net.HttpCookie;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@Category(WebCategory.class)
public class TaskRestEndpointClientTest {

    @NotNull
    private static final String TASK_URL = "http://localhost:8080/api/task/";

    @NotNull
    private static final HttpHeaders HEADER = new HttpHeaders();

    @NotNull
    private final Task task = new Task();

    @BeforeClass
    public static void beforeClass() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url = "http://localhost:8080/api/auth/login?username=admin&password=admin";
        HEADER.setContentType(MediaType.APPLICATION_JSON);
        @NotNull final ResponseEntity<Result> response =
                restTemplate.postForEntity(url, new HttpEntity<>(""), Result.class);
        Assert.assertEquals(200, response.getStatusCodeValue());
        Assert.assertNotNull(response.getBody());
        Assert.assertTrue(response.getBody().getSuccess());
        @NotNull final HttpHeaders headersResponse = response.getHeaders();
        @NotNull final List<HttpCookie> cookies = HttpCookie.parse(
                headersResponse.getFirst(HttpHeaders.SET_COOKIE)
        );
        @Nullable final String sessionId = cookies.stream()
                .filter(item -> "JSESSIONID".equals(item.getName()))
                .findFirst().get().getValue();
        Assert.assertNotNull(sessionId);
        HEADER.put(HttpHeaders.COOKIE, Arrays.asList("JSESSIONID=" + sessionId));
    }

    private static <T> ResponseEntity<T> sendRequest(
            @NotNull final String url,
            @NotNull final HttpMethod method,
            @NotNull final HttpEntity httpEntity,
            @NotNull final Class<T> responseType
    ) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        return restTemplate.exchange(url, method, httpEntity, responseType);
    }

    @Before
    public void init() {
        @NotNull final String url = TASK_URL + "save/";
        sendRequest(url, HttpMethod.POST, new HttpEntity<>(task, HEADER), Task.class);
    }

    @After
    public void destroy() {
        @NotNull final String url = TASK_URL + "clear/";
        sendRequest(url, HttpMethod.DELETE, new HttpEntity<>(HEADER), Task.class);
    }

    @AfterClass
    public static void logout() {
        @NotNull final String logoutUrl = "http://localhost:8080/api/auth/logout";
        sendRequest(logoutUrl, HttpMethod.POST, new HttpEntity<>(HEADER), Result.class);
    }

    private long count() {
        @NotNull final String logoutUrl = TASK_URL + "count/";
        @NotNull final ResponseEntity<Long> response =
                sendRequest(logoutUrl, HttpMethod.GET, new HttpEntity<>(HEADER), Long.class);
        if (response.getStatusCode() != HttpStatus.OK) return 0;
        @Nullable Long result = response.getBody();
        if (result == null) return 0;
        return result;
    }

    @Test
    public void findAll() {
        @NotNull final String url = TASK_URL + "findAll/";
        @NotNull final ResponseEntity<List> response =
                sendRequest(url, HttpMethod.GET, new HttpEntity<>(HEADER), List.class);
        Assert.assertEquals(response.getStatusCode(), HttpStatus.OK);
        @Nullable final List tasks = response.getBody();
        Assert.assertNotNull(tasks);
        Assert.assertEquals(1, tasks.size());
    }

    @Test
    public void findById() {
        @NotNull final String url = TASK_URL + "findById/" + task.getId();
        @NotNull final ResponseEntity<Task> response =
                sendRequest(url, HttpMethod.GET, new HttpEntity<>(HEADER), Task.class);
        Assert.assertEquals(response.getStatusCode(), HttpStatus.OK);
        @Nullable final Task taskFind = response.getBody();
        Assert.assertNotNull(taskFind);
        Assert.assertEquals(task.getId(), taskFind.getId());
    }

    @Test
    public void existsById() {
        @NotNull String url = TASK_URL + "existsById/" + task.getId();
        @NotNull ResponseEntity<Boolean> response =
                sendRequest(url, HttpMethod.GET, new HttpEntity<>(HEADER), Boolean.class);
        Assert.assertEquals(response.getStatusCode(), HttpStatus.OK);
        @Nullable Boolean result = response.getBody();
        Assert.assertNotNull(result);
        Assert.assertTrue(result);

        url = TASK_URL + "existsById/" + UUID.randomUUID();
        response = sendRequest(url, HttpMethod.GET, new HttpEntity<>(HEADER), Boolean.class);
        Assert.assertEquals(response.getStatusCode(), HttpStatus.OK);
        result = response.getBody();
        Assert.assertNotNull(result);
        Assert.assertFalse(result);
    }

    @Test
    public void save() {
        @NotNull final String url = TASK_URL + "save/";
        @NotNull final Task taskSave = new Task();
        @NotNull final ResponseEntity<Task> response =
                sendRequest(url, HttpMethod.POST, new HttpEntity<>(taskSave, HEADER), Task.class);
        Assert.assertEquals(response.getStatusCode(), HttpStatus.OK);
        @NotNull final String findUrl = TASK_URL + "findById/" + taskSave.getId();
        @NotNull final ResponseEntity<Task> responseFind = sendRequest(findUrl, HttpMethod.GET, new HttpEntity<>(HEADER), Task.class);
        Assert.assertNotNull(responseFind.getBody());
        @NotNull final Task taskFind = responseFind.getBody();
        Assert.assertEquals(taskSave.getId(), taskFind.getId());
    }

    @Test
    public void delete() {
        Assert.assertEquals(1, count());
        @NotNull final String url = TASK_URL + "delete/";
        sendRequest(url, HttpMethod.POST, new HttpEntity<>(task, HEADER), Task.class);
        Assert.assertEquals(0, count());
    }

    @Test
    public void deleteAll() {
        Assert.assertEquals(1, count());
        List<Task> tasks = new ArrayList<>();
        tasks.add(task);
        @NotNull final String url = TASK_URL + "deleteAll/";
        sendRequest(url, HttpMethod.POST, new HttpEntity<>(tasks, HEADER), Task.class);
        Assert.assertEquals(0, count());
    }

    @Test
    public void clear() {
        Assert.assertEquals(1, count());
        @NotNull final String url = TASK_URL + "clear";
        sendRequest(url, HttpMethod.DELETE, new HttpEntity<>(HEADER), Task.class);
        Assert.assertEquals(0, count());
    }

    @Test
    public void deleteById() {
        Assert.assertEquals(1, count());
        @NotNull final String url = TASK_URL + "deleteById/" + task.getId();
        sendRequest(url, HttpMethod.DELETE, new HttpEntity<>(HEADER), Task.class);
        Assert.assertEquals(0, count());
    }

    @Test
    public void countTest() {
        Assert.assertEquals(1, count());
    }

}
